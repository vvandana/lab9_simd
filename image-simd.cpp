#include <hpcdefs.hpp>
#include <image.hpp>

void convert_to_floating_point_optimized(const uint8_t *CSE6230_RESTRICT fixed_point_images, double *CSE6230_RESTRICT floating_point_images, size_t image_width, size_t image_height, size_t image_count) {
	__m128d constant = _mm_set1_pd(1/255.0);
        int s_index=0;
        for (size_t image_number = 0; image_number < image_count; image_number++) {
                for (size_t image_row = 0; image_row < image_height; image_row++) {
                        int width_s = image_width;
                        for (; width_s >= 16; width_s -= 16) {
                                __m128i fixed_points = _mm_loadu_si128((const __m128i*)&fixed_point_images[s_index]);
                                __m128i zero8 = _mm_set1_epi8(0);
                                __m128i fixed_points16[2];
                                fixed_points16[0] = _mm_unpacklo_epi8(fixed_points, zero8);
                                fixed_points16[1] = _mm_unpackhi_epi8(fixed_points, zero8);
                                __m128i zero16 = _mm_set1_epi16(0);
                                __m128i fixed_points32[4];
                                fixed_points32[0] = _mm_unpacklo_epi16(fixed_points16[0], zero16);
                                fixed_points32[1] = _mm_unpackhi_epi16(fixed_points16[0], zero16);
                                fixed_points32[2] = _mm_unpacklo_epi16(fixed_points16[1], zero16);
                                fixed_points32[3] = _mm_unpackhi_epi16(fixed_points16[1], zero16);

                                __m128d floating_points_lo;
                                __m128d floating_points_hi;
                                for(int i=0; i<4; i++){
                                        floating_points_lo = _mm_cvtepi32_pd (fixed_points32[i]);
                                        floating_points_lo = _mm_mul_pd(floating_points_lo, constant);
                                        _mm_storeu_pd(&floating_point_images[s_index + (i*4)], floating_points_lo);
                                        
					fixed_points32[i] = _mm_srli_si128 (fixed_points32[i], 8);
                                        floating_points_hi = _mm_cvtepi32_pd (fixed_points32[i]);
					floating_points_hi = _mm_mul_pd(floating_points_hi, constant);
                                        _mm_storeu_pd(&floating_point_images[s_index + (i*4)+2], floating_points_hi);
                                }
                                s_index += 16;
                        }
                        for(;width_s>0; width_s--){
                                floating_point_images[s_index] = double(fixed_point_images[s_index]) / 255.0;
                                s_index++;
                        }
                }

	}
	/*int s_index=0;
	double constant = 1/255.0;
	for (size_t image_number = 0; image_number < image_count; image_number++) {
                for (size_t image_row = 0; image_row < image_height; image_row++) {
                        int width_s = image_width;
                        for (; width_s >= 16; width_s -= 16) {
				for(int i=0;i<4; i++){
				_mm_prefetch((char*)&fixed_point_images[s_index], _MM_HINT_T0);
				floating_point_images[s_index] = double(fixed_point_images[s_index]) * constant;
				floating_point_images[s_index+1] = double(fixed_point_images[s_index+1]) * constant;
				floating_point_images[s_index+2] = double(fixed_point_images[s_index+2]) * constant;
				floating_point_images[s_index+3] = double(fixed_point_images[s_index+3]) * constant;
				s_index+=4;
				}
			}
			for(;width_s>0; width_s--){
                                floating_point_images[s_index] = double(fixed_point_images[s_index]) * constant;
                                s_index++;
                        }
		}
	}*/
}

void matrix_vector_multiplication_optimized(double *CSE6230_RESTRICT output_vector, const double *CSE6230_RESTRICT matrix, const double *CSE6230_RESTRICT input_vector, size_t matrix_width, size_t matrix_height) {
	int j_index = 0;
	int vec_size = matrix_width/2;
	__m128d input_vector_simd[vec_size];
	for (int j=0, width_s = matrix_width; width_s>=2, j<vec_size; width_s-=2,j++) {
		input_vector_simd[j]= _mm_load_pd(&input_vector[j_index]);
		j_index += 2;
	}
	int s_index = 0;
	for (size_t i = 0; i < matrix_height; i++) {
		__m128d accumulated_sum = _mm_set1_pd(0.0);
		__m128d zero = _mm_set1_pd(0.0);
		int width_s = matrix_width, j=0;
		__m128d matrix_simd1, matrix_simd2,matrix_simd3, matrix_simd4;
		//_mm_prefetch(((char*)&input_vector_simd[j]), _MM_HINT_T0);
		for (; width_s>=8; width_s-=8) {
			matrix_simd1 = _mm_loadu_pd(&matrix[s_index]);
			matrix_simd2 = _mm_loadu_pd(&matrix[s_index+2]);
			matrix_simd3 = _mm_loadu_pd(&matrix[s_index+4]);
			matrix_simd4 = _mm_loadu_pd(&matrix[s_index+6]);
			//_mm_prefetch((char*)&input_vector[j], _MM_HINT_T0);
			accumulated_sum = _mm_add_pd(accumulated_sum, _mm_mul_pd(matrix_simd1, input_vector_simd[j]));
			accumulated_sum = _mm_add_pd(accumulated_sum, _mm_mul_pd(matrix_simd2, input_vector_simd[j+1]));
			accumulated_sum = _mm_add_pd(accumulated_sum, _mm_mul_pd(matrix_simd3, input_vector_simd[j+2]));
                        accumulated_sum = _mm_add_pd(accumulated_sum, _mm_mul_pd(matrix_simd4, input_vector_simd[j+3]));
			s_index += 8;
			j+=4;
		}
		accumulated_sum = _mm_add_pd(_mm_unpacklo_pd(accumulated_sum, zero), _mm_unpackhi_pd(accumulated_sum, zero));
		double * accumulated_sum_doubles = (double*) &accumulated_sum;
		j_index = j*2;
		for(;width_s>0, j_index<matrix_width; width_s--){
			accumulated_sum_doubles[0] += matrix[s_index] * input_vector[j_index]; 
			s_index++;
			j_index++;
		}	 
		output_vector[i] = accumulated_sum_doubles[0];
	}
/*
	int s_index=0;
	for(size_t i = 0; i < matrix_height; i++){
		int length = matrix_width,N=0;
		double sum0 = 0.0, sum1 = 0.0;
		double sum2 = 0.0, sum3 = 0.0;
		double sum4 = 0.0, sum5 = 0.0;
		double sum6 = 0.0, sum7 = 0.0;
		for (; length >= 8; length -= 8) {
			_mm_prefetch((char*)&input_vector[8 * N], _MM_HINT_T0);
			_mm_prefetch((char*)&matrix[s_index], _MM_HINT_T0);
			sum0 += input_vector[8*N]* matrix[s_index];
			sum1 += input_vector[8*N+1]* matrix[s_index+1];
			sum2 += input_vector[8*N+2]* matrix[s_index+2];
			sum3 += input_vector[8*N+3]* matrix[s_index+3];
			sum4 += input_vector[8*N+4]* matrix[s_index+4];
                        sum5 += input_vector[8*N+5]* matrix[s_index+5];
                        sum6 += input_vector[8*N+6]* matrix[s_index+6];
                        sum7 += input_vector[8*N+7]* matrix[s_index+7];
			N++;
			s_index+=8;
		}
		double accumulated_sum = sum0 + sum1 + sum2 + sum3 + sum4 + sum5 + sum6 + sum7;
		N*=8;
		for(;N<matrix_width; N++){
			accumulated_sum = accumulated_sum + (input_vector[N]* matrix[s_index]);
			s_index++;
		}
		output_vector[i] = accumulated_sum;
	}*/
}


